'''
Created on 18.01.2019

@author: xets
'''
from xml.dom.minidom import parse
from xml.dom import minidom  
'''
interface OutputFormatter {
    /** the possible values for format control */
'''
FC_SGL = 1 # single element
FC_BEG = 2 # begin of subtree (current node descends)
FC_END = 3 # subtree completion (current node ascends)
'''
    public function initResult();
    public function formatElement($form_ctl, $elem = null, $value = null, $attrs = array());
    // $form_ctl: format control, see above
    // $elem: label (not for FC_END)
    // $value, if present
    // $attrs, key value pairs, e.g. "id => 1", "mod => 3"
    public function finish();
    public function printIt();
}
'''
class XmlFormatter:
    
    '''
    private data:
    xml
    currentNode
    currentParents
    '''
    currentParents = {}
    
    def initResult(self):
        impl = minidom.getDOMImplementation()
        self.xml = impl.createDocument(None, None, None)
        self.currentNode = self.xml
        self.currentParents = []
    
    def formatElement(self, form_ctl, elem = None, value = None, attrs = {}):
        if form_ctl == FC_END:
            self.currentNode = self.currentParents.pop()
            return

        elemNode = self.xml.createElement(elem)

        if value is not None:
            elemNode.nodeValue = value

        attr_keys = attrs.keys()
        for attr_key in  attr_keys:
            #attr = self.xml.createAttribute(attr_key)
            #attr.nodeValue = attrs[attr_key]
            #elemNode.appendChild(attr)
            elemNode.setAttribute(attr_key, attrs[attr_key])

        self.currentNode.appendChild(elemNode)
        
        if form_ctl == FC_BEG:
            self.currentParents.append(self.currentNode)
            self.currentNode = elemNode
    
    def finish(self):
        dummy = 1
    
    def printIt(self):
        return self.xml.toprettyxml()
