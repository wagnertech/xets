'''
Created on 18.01.2019

@author: xets
'''
from xml.dom import minidom
# import xml

'''
the possible values for extract control
'''
EC_BEG = 1  # begin of subtree (current node descends) - in
EC_CTN = 2  # continue with next sibling element - in / out: data of found element 
EC_END = 3  # subtree completion (current node ascends) - in / out: no data
            # results in EC_END out without data
'''
functions:
    public function openInput(&$input);
    public function toTop();    // navigation: current node := top level node
    public function toParent();    // navigation: current node := parent node
    public function extractElement($extr_ctl, &$elem, &$value, &$attrs);
    // $extr_ctl: extract control, see above
    // $elem: label (not on EC_END)
    // $value, if present
    // $attrs, key value pairs, e.g. "id => 1", "mod => 3"
    // returns extract control
'''


class XmlExtractor:
    '''
    private data:
    docNode
    currentNode
    '''    

    def openInput(self, input_file_name):
        self.docNode = minidom.parse(input_file_name)
        self.currentNode = self.docNode
        
    def reset(self):
        self.currentNode = self.docNode

    def extractElement(self, extr_ctl):
        
        # return parameters
        elem = ''
        value = None
        attrs = {}
        
        if extr_ctl == EC_BEG:
            srh_node = self.currentNode.firstChild
        elif extr_ctl == EC_CTN:
            srh_node = self.currentNode.nextSibling
        else:
            self.currentNode = self.currentNode.parentNode
            return (EC_END, elem, value, attrs)

        # search for next element node
        while srh_node is not None:
            if srh_node.nodeType != srh_node.ELEMENT_NODE:
                srh_node = srh_node.nextSibling
            else:
                break
        
        if srh_node is not None:
            # element node found: extract tag name, value and attributes
            self.currentNode = srh_node
            elem = self.currentNode.tagName
            if self.currentNode.nodeValue is not None: 
                value = self.currentNode.nodeValue
            else:
                # search for CDATA or TEXT_NODE section
                for childNode in self.currentNode.childNodes:
                    if childNode.nodeType == childNode.CDATA_SECTION_NODE:
                        value = childNode.data
                    elif childNode.nodeType == childNode.TEXT_NODE:
                        if len(childNode.data.strip()) > 0:
                            value = childNode.data
            for att_idx in range(0, self.currentNode.attributes.length):
                att_node = self.currentNode.attributes.item(att_idx)    
                attrs[att_node.name] = att_node.value
            return (EC_CTN, elem, value, attrs)
        else:
            # terminate / ascend subtree after child extraction
            if extr_ctl == EC_CTN:
                self.currentNode = self.currentNode.parentNode
            #elif extr_ctl == EC_END:
            #    self.currentNode = self.currentNode.parentNode.parentNode
            return (EC_END, elem, value, attrs)
    '''        
    def processLevel(self):
        '' returns a array with a element for each entry of this XML layer.
            Each elements points to a map with 4 elements:
            elem  => element name
            attrs => a map containing the attributes of that element
            value => a string with the text content
            items => a array containing the elements of the next layer ''
        
        (ctl,elem, value, attrs) = self.extractElement(EC_BEG)
        result = []
        while ctl != EC_END:
            items = self.processLevel()
            result.append = {"elem" : elem,
                            "attrs" : attrs,
                            "value" : value,
                            "items" : items}
            (ctl,elem, value, attrs) = self.extractElement(ctl)
        return result
    '''
    def processLevel(self):
        ''' returns a map with a element for each entry of this XML layer.
            The map value is an array (XML entries are not unique).
            Each array element points to a map with 3 elements:
            attrs => a map containing the attributes of that element
            value => a string with the text content
            items => a map containing the elements of the next layer '''
        
        (ctl,elem, value, attrs) = self.extractElement(EC_BEG)
        result = {}
        while ctl != EC_END:
            items = self.processLevel()
            entry = {"attrs" : attrs,
                    "value" : value,
                    "items" : items}
            if elem in result:
                # element was already existing
                result[elem].append(entry)
            else:
                result[elem] = [entry];
            (ctl,elem, value, attrs) = self.extractElement(ctl)
        return result

    def readFile(self, input_file_name):
        self.openInput(input_file_name)
        return self.processLevel()

    def gotoChild(self, name):
        # return True/False, if child node could be found
        
        ctl, elem, dummy, dummy1 = self.extractElement(EC_BEG)
        while (ctl != EC_END):
            if elem == name:
                return True
            ctl, elem, dummy, dummy1 = self.extractElement(ctl)
        return False

    def requireChild(self, name):
        
        # throws exception, if child not present
        if not self.gotoChild(name):
            raise EnvironmentError("Could not find node "+name)
    
