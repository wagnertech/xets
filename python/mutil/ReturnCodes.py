'''
Created on 03.12.2019

@author: mint18
'''
import enum

class ReturnCodes(enum.IntEnum):
    OK = 0
    COMMAND_ERROR = 1
    EXIST = 2
    INVALID_VALUE = 3
    