'''
Created on 04.05.2019

@author: xets
'''

class Visulizer(object):
    '''
    classdocs
    '''


    def __init__(self, step):
        '''
        Constructor
        '''
        self.indent = 0
        self.step = step
        
    def setHeader(self, h):
        print ("#################################################")
        print (h)
        print ("#################################################")
        
    def startParameterBlock(self, name):
        print (self.indent*" "+"------------------------------------------")
        print (self.indent*" "+name)
        print (self.indent*" "+"------------------------------------------")
        self.indent += self.step
        
    def endParameterBlock(self):
        if self.indent > 0:        
            self.indent -= self.step
            print (self.indent*" "+"------------------------------------------")
            
    def parameterSeparator(self, text):
        if text.strip():
            print (self.indent*" "+"-- "+text+" --")
        else:
            print()

    def showParameter(self, name, parameter, parameterType, value):
        if parameter["Text"].strip():
            if "SuffixText" in parameter and parameter["SuffixText"]:
                print (self.indent*" "+parameter["Text"]+": "+value+parameter["SuffixText"]+"; "+name)
            elif parameterType["type"] == "TypeRestriction":
                # enumeration handling
                print (self.indent*" "+parameter["Text"]+": "+
                       parameterType["Enumeration"][value]["Text"]+"; "+name)
            elif parameterType["type"] == "TypeText":
                print (self.indent*" "+parameter["Text"]+': "'+value+'"; '+name)
            else:
                raise RuntimeError("Unknown parameter type: "+parameterType["type"])
        
    def showParameterType(self, parameter_ref, parameter_type):
        print (self.indent*" "+"ParameterRef: "+parameter_ref)
        print (self.indent*" "+"Name: "+parameter_type["Name"])
        self.indent += self.step
        if parameter_type["type"] == "TypeRestriction":
            for value in parameter_type["Enumeration"]:
                print (self.indent*" "+value+": "+parameter_type["Enumeration"][value]["Text"])
        elif parameter_type["type"] == "TypeNumber":
            print (self.indent*" "+"TypeNumber: "+parameter_type["attrs"]["minInclusive"]
                   +" .. "+parameter_type["attrs"]["maxInclusive"])
        elif parameter_type["type"] == "TypeText":
            print (self.indent*" "+"TypeText: Size (bit) = "+parameter_type["attrs"]["SizeInBit"])
        else:
            print (self.indent*" "+"unknown type")
        
        