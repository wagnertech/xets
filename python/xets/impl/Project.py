'''
Created on 18.01.2019

@author: xets
'''
import os
import pickle
from xets.util import XmlFormatter
from xets.util.XmlExtractor import XmlExtractor
from xets.util import XmlExtractor as XMLE

def create(name, project_dir):

    # check existence of project dir
    if not os.path.exists(project_dir):
        os.mkdir(project_dir)
        
    # change to project dir
    os.chdir(project_dir)
    
    # check existence of project file
    if os.path.exists(".project"):    
        raise RuntimeError("project already existing in "+project_dir)
    
    # create sub dirs
    os.mkdir("file")
    os.mkdir("product")
    os.mkdir("prototype")
    os.mkdir("device")
    
    xmlFormatter = XmlFormatter.XmlFormatter()
    xmlFormatter.initResult()
    xmlFormatter.formatElement(XmlFormatter.FC_BEG, "project", None, {"name" : name})
    xmlFormatter.formatElement(XmlFormatter.FC_END)
    s = xmlFormatter.printIt()
    with open('.project', 'w') as stream:
        stream.write(s)

    p = Project(project_dir)
    p.hello()
    p.store()

    return 0

def open_project(project_dir):
    p = Project(project_dir)
    p.hello()
    p.store()

    return 0

def files_in():
    return 0, os.listdir("file")

def products_in():
    return 0, os.listdir("product")

def prototypes_in():
    return 0, os.listdir("prototype")

def getUserHome():
    user_home = os.getenv("HOME")
    if user_home == None:
        raise RuntimeError("'HOME' environment not set")
    
    return user_home

class Project:
    '''
    private data:
    name
    dir
    user_home
    '''
    
    def __init__(self, dir=None, name=None):
        if name == None and dir == None:
            # load instance from .xets file
            with open(getUserHome()+"/.xets", "rb") as stream:
                p = pickle.load(stream)
                self.name      = p.name
                self.dir       = p.dir
                self.user_home = p.user_home
        elif name == None:
            # store working dir
            cwd = os.getcwd()
        
            # change to project dir
            if not os.path.exists(dir):
                raise ValueError("Project dir does not exist: "+dir)
            os.chdir(dir)
            
            # check existence of project file
            if not os.path.exists(".project"):    
                raise ValueError("project missing in "+dir)
        
            xmlex = XmlExtractor()
            xmlex.openInput(".project")
            ctl = XMLE.EC_BEG
            (ctl,elem, value, attrs) = xmlex.extractElement(ctl)
            
            self.dir = dir
            self.name = attrs['name']
            self.user_home = getUserHome()
            os.chdir(cwd)
            
        else:
            raise RuntimeError("Unknown parameter combination")        
            
    def store(self):
        
        # write project file
        with open(self.user_home+"/.xets", "wb") as stream:
            pickle.dump(self, stream)

    def hello(self):
        print ("Current project:")
        print ("name : "+self.name)
        print ("dir  : "+self.dir)
        os.chdir(self.dir)

    def getDir(self):
        return self.dir
    
    def getUserHome(self):
        return self.user_home
    