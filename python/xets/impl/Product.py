'''
Created on 20.04.2019

@author: xets
'''

import os

from xets.util.XmlExtractor import XmlExtractor
from xets.util import XmlExtractor as XMLE
import xets.util.ReturnCodes as RC

from xets.impl.KnxFile import KnxFile

def pimport(file_name, refid):
    f = KnxFile(file_name)
    product_list = f.getProductList()
    product_file = product_list[refid]["RefId"]
    if product_file is None:
        return 1 # not found
    os.makedirs("product/"+refid, 0o0755, True)
    os.system("cp file/"+f.file_name+"/M-0083/"+product_file+".xml product/"+refid+"/Product.xml") # TODO
    return 0
    
def extractParameterTypes(xmlex):
    parameterTypes = {}
    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # 1st parameter
    while ctl != XMLE.EC_END:
        ptype = extractParameterType(xmlex)
        ptype["Name"] = attrs["Name"]
        parameterTypes[attrs["Id"]] = ptype
        (ctl,elem, value, attrs) = xmlex.extractElement(ctl) # next parameter
    return parameterTypes
    
def extractParameterType(xmlex):
    '''
    ParameterType structure:
    ["type"]: main type
    ["attrs"]: type attributes
    [...]: further information
    '''
    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Level 0
    if elem == "TypeRestriction": 
        parameterType = {"type":"TypeRestriction", "attrs":attrs, "Enumeration" : {} }
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Enumeration 
        while ctl != XMLE.EC_END:
            parameterType["Enumeration"][attrs["Value"]] = attrs
            (ctl,elem, value, attrs) = xmlex.extractElement(ctl) # next parameter
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # End TypeRestriction
    elif elem == "TypeNumber":
        parameterType = {"type":"TypeNumber", "attrs" : attrs }
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # End TypeNumber
    elif elem == "TypeText":
        parameterType = {"type":"TypeText", "attrs" : attrs }
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # End TypeText
    else:
        parameterType = {}
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # End Any
    return parameterType
    
    
# Multiton implementation
products = {}

def getProduct(pid):
    if pid not in products:
        products[pid] = Product(pid)
    return products[pid]

class Product(object):
    '''
    private data:
    product_id
    parameters
    parameterrefs
    parameterTypes
    xmlextractor     # loaded XML document
    '''

    def __init__(self, product_id):
        '''
        Constructor
        '''
        # check existence
        if not os.path.exists("product/"+product_id+"/Product.xml"):
            raise ValueError("Product not existing: ", product_id)
        
        self.product_id = product_id
        
        # offne (lesend) XML von PRODUCT
        self.xmlextractor = XmlExtractor()
        self.xmlextractor.openInput("product/"+product_id+"/Product.xml")
        #gehe zu <Parameters>
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # KNX
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # ManufacturerData
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # Manufacturer
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # ApplicationPrograms
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # ApplicationProgram
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # Static
        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # level 0
        #Lege leere parameters-Map an
        self.parameters = {}
        self.parameterrefs = {}
        while ctl != XMLE.EC_END:
            if elem == "ParameterTypes":
                self.parameterTypes = extractParameterTypes(self.xmlextractor)
            elif elem == "Parameters":
                #Lese alle Kind-Elemente
                (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # level 1
                while ctl != XMLE.EC_END:
                    #Prufe Element == Parameter
                    if elem == "Parameter":
                        #Fuge parameters zum Schlussel Id alle attrs hinzu
                        self.parameters[attrs["Id"]] = attrs
                    elif elem == "Union":
                        # Schleife uber die darunter liegenden Parameter
                        (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # level 2
                        while ctl != XMLE.EC_END:
                            #Prufe Element == Parameter
                            if elem == "Parameter":
                                #Fuge parameters zum Schlussel Id alle attrs hinzu
                                self.parameters[attrs["Id"]] = attrs
                            (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_CTN) # level 2
                    (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_CTN) # level 1
            elif elem == "ParameterRefs":
                (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_BEG) # ParameterRef, level 1
                while ctl != XMLE.EC_END:
                    self.parameterrefs[attrs["Id"]] = attrs["RefId"]
                    (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_CTN) # level 1
            (ctl,elem, value, attrs) = self.xmlextractor.extractElement(XMLE.EC_CTN) # level 0

    def getParameters(self):
        return self.parameters

    def getParameterRefs(self):
        return self.parameterrefs
    
    def getParameter(self, parameter_ref):
        return self.parameters[self.parameterrefs[parameter_ref]]

    def getParameterType(self, parameter_ref):
        return self.parameterTypes[self.parameters[self.parameterrefs[parameter_ref]]["ParameterType"]]
    
    def getDefaultValues(self):
        parameters = self.getParameters()
        values = {}
        for parameter in parameters.values():
            values[parameter["Id"]] = parameter["Value"]
        return values       

    def getXmlExtractor(self):
        self.xmlextractor.reset()
        return self.xmlextractor
    
    def showParameter(self, parameter_ref, visu):
        ptype = self.getParameterType(parameter_ref)
        visu.showParameterType(parameter_ref, ptype)
        return 0

    def checkParameter(self, parameter_ref, value):
        ptype = self.getParameterType(parameter_ref)
        if ptype["type"] == "TypeRestriction":
            return value in ptype["Enumeration"]
        elif ptype["type"] == "TypeNumber":
            return int(ptype["attrs"]["minInclusive"]) <= int(value) <= int(ptype["attrs"]["maxInclusive"])
        elif ptype["type"] == "TypeText":
            return len(value)*8 <= int(ptype["attrs"]["SizeInBit"])
        else:
            raise RuntimeError("Unknown type: "+ptype["type"])
        
        
