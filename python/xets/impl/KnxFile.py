'''
Created on 20.04.2019

@author: xets
'''

import os
import re
import zipfile

from xets.util.XmlExtractor import XmlExtractor
from xets.util import XmlExtractor as XMLE

from xets.impl import Project

def fimport(project, file_path):
    # file_path needs to be an absolute file path
    if file_path[0] != "/":
        # it's not an absolute path
        raise RuntimeError("file_path needs to be an absolute file path")
    
    # TODO: knxprod_name = re.sub(".*/(.*)\.knxprod", file_path, "%1")
    knxprod_name = re.sub(".*/", "", file_path)
    knxprod_name = re.sub("\.knxprod", "", knxprod_name)
    
    with zipfile.ZipFile(file_path, 'r') as prodfile:
        prodfile.extractall(project.getDir()+"/file/"+knxprod_name)
    
    return 0
'''
def fopen(project, file_name):
    if project == None:
        project = Project.Project()
    f = KnxFile(file_name)
    f.store(project)
    return 0
'''    

def products_in(file_name):
    f = KnxFile(file_name)
    return 0, f.getProductList()

class KnxFile(object):
    '''
    private data:
    file_name
    product_list
    '''


    def __init__(self, file_name):
        '''
        Constructor
        '''
        self.file_name = file_name
        
        cwd = os.getcwd()
        os.chdir("file/"+file_name+"/M-0083")  # TODO wie krieg ich das raus?
        xmlex = XmlExtractor()
        xmlex.openInput("Hardware.xml")
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # KNX
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # ManufacturerData
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Manufacturer
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Hardware
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Hardware (level 0)
        
        self.product_list = {}
        while ctl != XMLE.EC_END:
            id = attrs["Id"]
            self.product_list[id] = { "Name": attrs["Name"] }
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 1
            while ctl != XMLE.EC_END:
                if elem == "Hardware2Programs":
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Hardware2Program, level 2
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 3
                    while ctl != XMLE.EC_END:
                        if elem == "ApplicationProgramRef":
                            self.product_list[id]["RefId"] = attrs["RefId"]
                        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # next level 3
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # exit Hardware2Program, level 2
                (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # next level 1
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # next level 0
            
        os.chdir(cwd)
        
    def getProductList(self):
        return self.product_list 
            