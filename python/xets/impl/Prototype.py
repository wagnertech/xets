'''
Created on 21.04.2019

@author: xets
''' 
import os

from xets.util.XmlExtractor import XmlExtractor
from xets.util import XmlExtractor as XMLE
from xets.util.XmlFormatter import XmlFormatter
from xets.util import XmlFormatter as XMLF
from xets.util.ReturnCodes import ReturnCodes as RC, ReturnCodes

from xets.impl.Product import Product
from xets.impl import Product as PRD
from xets.impl.ValueCollector import ValueCollector

def eval_parameters(xmlex, xmlform, parameter_refs, values):
    #Lese alle Kind-Elemente
    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 0
    while ctl != XMLE.EC_END:
        if elem == "Channel":
            #Schreibe <Channel ...>
            xmlform.formatElement(XMLF.FC_BEG, elem, value, attrs)
            eval_parameters(xmlex, xmlform, parameter_refs, values)
            xmlform.formatElement(XMLF.FC_END)
        elif elem == "ChannelIndependentBlock":
            # is ignored
            eval_parameters(xmlex, xmlform, parameter_refs, values)
        elif elem == "ParameterBlock":
            #Schreibe <ParameterBlock ...>
            xmlform.formatElement(XMLF.FC_BEG, elem, value, attrs)
            eval_parameters(xmlex, xmlform, parameter_refs, values)
            xmlform.formatElement(XMLF.FC_END)

        #Wenn Element == ParameterRefRef
        elif elem == "ParameterRefRef":
            #Hole uber ParameterRef RefId
            refId = parameter_refs[attrs["RefId"]]
            #Lese Value
            value = values[refId]
            #Schreibe ParameterRefRef incl. Value
            attrs["CurrValue"] = value
            xmlform.formatElement(XMLF.FC_BEG, elem, value, attrs)
            xmlform.formatElement(XMLF.FC_END)
            
        elif elem == "choose":
            # search "ParameterRefRef incl. Value" and fetch value
            refId = parameter_refs[attrs["ParamRefId"]]
            pval = values[refId]
            # read child elements
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 1
            while ctl != XMLE.EC_END:
                #Wenn Element == when && es existiert ein "test"-Attribut && Wert des Test-Attributs == Value
                if elem == "when" and "test" in attrs and attrs["test"] == pval:
                    eval_parameters(xmlex, xmlform, parameter_refs, values)
                    #exit Choose
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # exit level 1
                #Wenn Element == when && es existiert ein "default"-Attribut
                elif elem == "when" and "default" in attrs:
                    eval_parameters(xmlex, xmlform, parameter_refs, values)
                    #exit Choose
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_END) # exit level 1
                else:
                    (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # level 1
        #Wenn Element == ParameterSeparator
        elif elem == "ParameterSeparator":
            #Schreibe Element ParameterSeparator
            xmlform.formatElement(XMLF.FC_BEG, elem, value, attrs)
            xmlform.formatElement(XMLF.FC_END)
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # level 0
'''
    create a prototype
    name:       name of (new) prototype
    product_id: id of (existing) product
    The prototype has the default values as defined in the product.
'''
def pcreate(name, product_id):
    # check if already existing
    if os.path.exists("prototype/"+name):
        raise ValueError("Prototype existing: "+ name)

    
    product = PRD.getProduct(product_id)

    proto = Prototype(name, product=product)
    return 0

class Prototype(object):
    '''
    classdocs
    '''
    name = ""       # name of prototype
    filename = ""   # filename of prototype XML file
    product = None  # related product
    values = {}     # list of current values

    def __init__(self, name, product=None):
        '''
        Constructor
        '''
        self.name = name
        self.filename = "prototype/"+name+"/"+name+".xml"
        
        if product:
            # create prototype from product
            self.product = product
            self.values = product.getDefaultValues()
            self.writeXml()
        else:
            # create prototype from file
            
            # extract product name from xml file
            if not os.path.exists(self.filename):
                raise ValueError("Prototype does not exist: "+self.name)
            xmlex = XmlExtractor()
            xmlex.openInput(self.filename)
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Prototyp
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Product
            
            self.product = PRD.getProduct(attrs['name'])
            
            # set values
            self.values = self.product.getDefaultValues()
            valueCollector = ValueCollector()
            if self.show(valueCollector) != 0:
                raise RuntimeError("Could not collect values")
            self.values.update(valueCollector.values)
        
    def writeXml(self):
        #Lege Verzeichnis fur prototyp NAME an
        os.makedirs("prototype/"+self.name, 0o0755, True)
    
        #offne (schreibend) XML von prototyp NAME
        xmlFormatter = XmlFormatter()
        xmlFormatter.initResult()
        #Schreibe <Prototyp name=NAME>
        xmlFormatter.formatElement(XMLF.FC_BEG, "Prototyp", None, {"name" : self.name})
        #Schreibe <Produkt name=PRODUKT/>
        xmlFormatter.formatElement(XMLF.FC_BEG, "Product", None, {"name" : self.product.product_id})
        xmlFormatter.formatElement(XMLF.FC_END)
        #offne (lesend) XML von PRODUCT
        xmlex = self.product.getXmlExtractor()
        #gehe zu <ChannelIndependentBlock>
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # KNX
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # ManufacturerData
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Manufacturer
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # ApplicationPrograms
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # ApplicationProgram
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 0
        while ctl != XMLE.EC_END:
            if elem == "Dynamic":
                eval_parameters(xmlex, xmlFormatter, self.product.getParameterRefs(), self.values)
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # level 0
    
        xmlFormatter.formatElement(XMLF.FC_END) # Prototyp
        
        s = xmlFormatter.printIt()
        with open(self.filename, 'w') as stream:
            stream.write(s)
        
    def show(self, visulizer):
        
        visulizer.setHeader(self.name+" of "+self.product.product_id)

        #offne (lesend) XML von PROTOTYPE
        xmlex = XmlExtractor()
        xmlex.openInput(self.filename)
        #gehe zu <Parameters>
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # Prototyp
        self.show_parameters(xmlex, visulizer)
        return 0
    
    def show_parameters(self, xmlex, visulizer):
        (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_BEG) # level 0
        while ctl != XMLE.EC_END:
            if elem == "ParameterBlock":
                visulizer.startParameterBlock(attrs["Text"])
                self.show_parameters(xmlex, visulizer)
            elif elem == "Channel":
                visulizer.startParameterBlock(attrs["Text"])
                self.show_parameters(xmlex, visulizer)
            elif elem == "ParameterSeparator":
                visulizer.parameterSeparator(attrs["Text"])
            elif elem == "ParameterRefRef":
                visulizer.showParameter( \
                    attrs["RefId"], \
                    self.product.getParameter(attrs["RefId"]), \
                    self.product.getParameterType(attrs["RefId"]), \
                    attrs["CurrValue"])
            (ctl,elem, value, attrs) = xmlex.extractElement(XMLE.EC_CTN) # level 0
        visulizer.endParameterBlock()
    
    def set_parameter(self, parameter_refref, value):
        if not self.product.checkParameter(parameter_refref, value):
            return ReturnCodes.INVALID_VALUE
        refId = self.product.getParameterRefs()[parameter_refref]
        self.values[refId] = value
        self.writeXml()
        return ReturnCodes.OK
        
    