'''
Created on 03.12.2019

@author: mint18
'''

from xets.impl.NullVisulizer import NullVisulizer

class ValueCollector(NullVisulizer):
    '''
    classdocs
    '''
    values = {}
        
    def showParameter(self, name, parameter, parameterType, value):
        self.values[name] = value
