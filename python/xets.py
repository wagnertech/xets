#!/usr/bin/python3
'''
Created on 18.01.2019

@author: xets
'''
import os
import sys
import time

from xets.impl import KnxFile
from xets.impl import Product as PRODUCT
from xets.impl import Project as PROJECT
from xets.impl.Project import Project
from xets.impl import Prototype as PROTOTYPE
from xets.impl.Prototype import Prototype

from xets.cli.Visulizer import Visulizer

from mutil.ReturnCodes import ReturnCodes


'''
needed modules
- xml.dom
'''

'''
Anmerkung: Es gibt getopt.getopt()
'''

usage = '''
xets VERB OBJECT [PARAMS ...]
OBECT
    VERB [PARAMS ...]
project
    create NAME [DIR]
    open [DIR]
    files_in
    products_in
    prototypes_in
file
    import FILE_PATH
    products_in FILE_NAME
product
    import FILE_NAME ID
    show_parameter ID PARAMETER_REF
prototype
    create NAME PRODUCT_ID
    show NAME
    set_parameter NAME PARAMETER_REF VALUE
'''

def command_dispatch(args):
    
    # check runtime parameters
    if len(args) < 2:
        raise ValueError("xets needs at least 2 arguments.")
    
    # store working dir
    cwd = os.getcwd() + "/"
    
    if args[1] == "project" and (args[0] == "create" or args[0] == "open"):
        # these commands define the current project. For all other
        # commands the project object has to be present in ~/.xets
        i=1 # dummy command
    else:
        project = Project()
        project.hello()
    
    if args[1] == "project":
        if args[0] == "create":
            if len(args) < 3:
                raise ValueError("'create project' needs at least 3 arguments.")
            name = args[2]
            if len(args) == 4:
                project_dir = args[3]
            else:
                project_dir = os.getcwd()
            return PROJECT.create(name, project_dir)
        elif args[0] == "open":
            if len(args) == 3:
                project_dir = args[2]
            else:
                project_dir = os.getcwd()
            return PROJECT.open_project(project_dir)
        elif args[0] == "files_in":
            r,files = PROJECT.files_in()
            print (files)
            return r
        elif args[0] == "products_in":
            r,p = PROJECT.products_in()
            print(p)
            return r
        elif args[0] == "prototypes_in":
            r,p = PROJECT.prototypes_in()
            print(p)
            return r
        else:
            raise ValueError("Unknown VERB for 'project'")
        
    elif args[1] == "file":
        if args[0] == "import":
            if len(args) != 3:
                raise ValueError("'import file' needs 3 arguments.")
            if args[2][0] == "/":
                # absolute path
                file_path = args[2]
            else:
                # make relative path absolute
                file_path = cwd + args[2]
            return KnxFile.fimport(project, file_path)
        elif args[0] == "products_in":
            if len(args) != 3:
                raise ValueError("'products_in file' needs 1 additional argument.")
            r,dirs =  KnxFile.products_in(args[2])
            print (dirs)
            return r
        else:
            raise ValueError("Unknown VERB for 'file'")

    elif args[1] == "product":
        if args[0] == "import":
            if len(args) != 4:
                raise ValueError("'import product' needs 2 additional arguments.")
            return PRODUCT.pimport(args[2], args[3])
        elif args[0] == "show_parameter":
            if len(args) != 4:
                raise ValueError("'show_parameter product' needs 2 additional arguments.")
            p = PRODUCT.getProduct(args[2])
            visu = Visulizer(2)
            return p.showParameter(args[3], visu)
        else:
            raise ValueError("Unknown VERB for 'product'")
    elif args[1] == "prototype":
        if args[0] == "create":
            if len(args) != 4:
                raise ValueError("'create prototype' needs 2 additional arguments.")
            return PROTOTYPE.pcreate(args[2], args[3])
        elif args[0] == "show":
            if len(args) != 3:
                raise ValueError("'show prototype' needs 1 additional argument.")
            proto = Prototype(args[2])
            visu = Visulizer(2)
            return proto.show(visu)
        elif args[0] == "set_parameter":
            if len(args) != 5:
                raise ValueError("'set_parameter prototype' needs 3 additional argument.")
            proto = Prototype(args[2])
            return proto.set_parameter(args[3], args[4])
        else:
            raise ValueError("Unknown VERB for 'prototype'")

    raise ValueError("invalid OBJECT "+args[1])

def main_loop():
    print ("Type command, 'help', or 'end' for END")
    inp = input()
    while (inp != "end"):
        if inp == "help":
            print (usage)
        else:
            try:
                cmd = inp.split()
                start = time.time()
                ret = command_dispatch(cmd)
                print ("Command finished after",time.time()-start,"seconds")
                if ret != 0:
                    print ("command execution returned: ", ret)
                print()
            except ValueError as ve:
                print (ve)

        print ("Type command, 'help', or <RET> for END")
        inp = input()
    
    return ReturnCodes.OK
            

if __name__ == '__main__':
    try:
        if len(sys.argv) == 1:
            # call without parameter -> start CLI
            ret = main_loop()
        else:
            # execute command directly
            ret = command_dispatch(sys.argv[1:])
            
        if ret != 0:
            print ("command execution returned: ", ret)
        exit (ret)
    except Exception as e:
        print (e)
        print (usage)
        raise
        #exit(99)
