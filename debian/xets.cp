#!/bin/bash
set -e

mkdir -p $1/usr/bin/
cp python/xets.py $1/usr/bin/xets

mkdir -p $1/usr/lib/python3/dist-packages/
cp -a python/xets $1/usr/lib/python3/dist-packages/

mkdir -p $1/usr/share/doc/xets
cp doc/* $1/usr/share/doc/xets

# copy man page
mkdir -p $1/usr/share/man/man1
gzip -c doc/xets.1 >$1/usr/share/man/man1/xets.1.gz

