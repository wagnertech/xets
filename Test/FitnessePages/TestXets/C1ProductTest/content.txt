!1 Product Management Functions

!2 Import Product

!|test.util.CallScript|
|call script|xets import product MDT_KP_Schaltaktor_AKS_AKI_Serie_03_V21a M-0083_H-26-2|

Check result

|call script|ls ~/TestProject/product/M-0083_H-26-2|

!2 Show Parameter

!|test.util.CallScript|
|call script|xets show_parameter product M-0083_H-26-2 M-0083_A-0016-21-3525_P-5000_R-5000 >ShowParameter.out|
|call script|grep "33: 33 s" ShowParameter.out|
